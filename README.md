GUIDE
============================

Install the Polymer 2 CLI.
-------------------

    npm install -g polymer-cli

And bower
-------------------

    npm install -g bower

Go to project directory
-------------------

    cd <project name>

Run
-------------------

    bower install

Run dev serve
-------------------

    polymer serve
